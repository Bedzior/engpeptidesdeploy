#!/bin/bash
DIR=$(pwd)
LOGFILE=$DIR/deploy.log
SERVER=$1
sudo cp startup/frontend.service /etc/systemd/system/frontend.service
sudo cp startup/backend.service /etc/systemd/system/backend.service

sudo systemctl daemon-reload
sudo service frontend start				>> $LOGFILE
sudo service backend start				>> $LOGFILE

echo "Server running in background"

sudo service apache2 reload				>> $LOGFILE
sudo service apache2 restart			>> $LOGFILE