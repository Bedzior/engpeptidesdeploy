#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
LOGFILE=provisioning.log
ANG_VERSION=$1
echo "[Vagrant] Updating system..."
sudo apt-get update												>> $LOGFILE
sudo apt-mark hold grub-efi-amd64								>> $LOGFILE
sudo apt-get upgrade -y											>> $LOGFILE
sudo apt-get -qq install curl -y								>> $LOGFILE
sudo apt-get install gnutls-bin -y								>> $LOGFILE
echo "[Vagrant] Installing Java and components..."
apt-get -qq install default-jdk -y								>> $LOGFILE
# apt-get -qq install sun-javadb-client sun-javadb-core -y		>> $LOGFILE

echo "[Vagrant] Installing git..."
sudo apt-get -qq install git -y									>> $LOGFILE
sudo ssh-keyscan -t dsa bitbucket.org >> .ssh/known_hosts
apt-get -qq install maven -y									>> $LOGFILE

echo "[Vagrant] Downloading jetty..."
cd /opt
wget --quiet -O jetty.tar.gz http://central.maven.org/maven2/org/eclipse/jetty/jetty-distribution/9.4.0.v20161208/jetty-distribution-9.4.0.v20161208.tar.gz >> $LOGFILE
tar -zxf jetty.tar.gz											>> $LOGFILE
sudo rm -rf jetty.tar.gz
mv jetty-distribution-9.4.0.v20161208 jetty9
sudo rm -rf jetty/demo*
sudo useradd --user-group --shell /bin/false --home-dir /opt/jetty9/temp jetty
sudo chown -R jetty:jetty /opt/jetty9
export JETTY_HOME=/opt/jetty9
export JETTY_BASE=/opt/jetty9
echo export JETTY_HOME=/opt/jetty9 >> ~/.bashrc
echo export JETTY_HOME=/opt/jetty9 >> ~/.profile
echo JETTY_HOME=/opt/jetty9 >> /etc/environment

echo "[Vagrant] Installing node and components..."
sudo apt-get install python-software-properties -y				>> $LOGFILE
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -	>> $LOGFILE
sudo apt-get install nodejs	--fix-missing -y					>> $LOGFILE
sudo apt-get remove curl -y										>> $LOGFILE
sudo apt-get install make -y									>> $LOGFILE
sudo npm install -g node-gyp@3.6.0								>> $LOGFILE
sudo npm install -g @angular/cli@latest							>> $LOGFILE

echo "[Vagrant] Installing apache2..."
apt-get -qq install apache2 -y									>> $LOGFILE
sudo a2enmod file_cache proxy proxy_http rewrite setenvif access_compat
sudo a2dissite 000-default
rm -rf /var/www/html
