#!/bin/bash
DIR=$(pwd)
LOGFILE=$DIR/provisioning.log
if [ ! -d /opt/peptides ]; then
	mkdir /opt/peptides
fi
cd /opt/peptides
if [ -d backend ]; then
	echo "Stopping jetty"
	sudo service jetty stop															>> $LOGFILE
	sudo rm -rf backend
fi
# sudo git clone git@bitbucket.org:Bedzior/engpeptidesbackend.git backend			>> $LOGFILE
sudo git clone https://Bedzior@bitbucket.org/Bedzior/engpeptidesbackend.git backend	>> $LOGFILE
cd backend
echo "Installing backend"
echo jetty.home=/opt/jetty9 >> maven/maven-ubuntu-config.properties
sudo ln config/apache/peptides.conf /etc/apache2/sites-available/peptides.conf		>> $LOGFILE
sudo a2ensite peptides																>> $LOGFILE

while [ ! -f /opt/jetty9/webapps/api.war ]; do
	echo "Recompiling backend..."
	sudo mvn clean compile test install -P linux-install,system-install				>> $LOGFILE
done
cd /opt/jetty9
echo "Making an initialising run of the backend part"
sudo java -jar start.jar -Dclean=true												>> $LOGFILE
sudo chown -R jetty:jetty /opt/jetty9