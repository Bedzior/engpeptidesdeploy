#!/bin/bash
DIR=$(pwd)
LOGFILE=$DIR/provisioning.log
if [ ! -d /opt/peptides ]; then
	mkdir /opt/peptides
fi
cd /opt/peptides
if [ -d frontend ];then
	PID=$(sudo lsof -t -i:4200)
	if [ ! -z "$PID" ]; then
		echo "Stopping angular applications running on port 4200"
		sudo kill $PID >> $LOGFILE
	fi
	rm -rf /opt/peptides/frontend
fi
# git clone git@bitbucket.org:Bedzior/engpeptidesfront.git frontend	>> $LOGFILE
git clone https://Bedzior@bitbucket.org/Bedzior/engpeptidesfront.git frontend	>> $LOGFILE
cd frontend

sudo npm install -g @angular/cli@latest						>> $LOGFILE
sudo npm install --production								>> $LOGFILE
