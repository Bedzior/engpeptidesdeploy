#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
LOGFILE=provisioning.log
HOST=$1
DOMAIN=$2
LANG=pl
sed -i "/127\.0\.1\.1/s/.*/127.0.1.1	$HOST.$DOMAIN	$HOST/" /etc/hosts
echo domain $DOMAIN > /etc/resolvconf/resolv.conf.d/base
resolvconf -u
service networking restart
echo "[Vagrant] Setting keyboard to $LANG"
apt-get -qq install x11-xkb-utils -y							>> $LOGFILE
sed -i "/^XKBLAYOUT/s/=".*"/=\"$LANG\"/" /etc/default/keyboard	>> $LOGFILE
sudo loadkeys $LANG												>> $LOGFILE
