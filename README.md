# README #

This simple README is here to silence BitBucket, mostly. 
### What is this repository for? ###

* Vagrant configuration and provisioning files (scripts)
* 1.0.0
### How do I get set up? ###

* Install [vagrant](https://www.vagrantup.com/downloads.html)
* Fetch this repository
* Make sure either Hyper-V (default provider) or VirtualBox are installed on your system
* Go to a folder on your computer you'd like to use as vagrant's home directory
* Launch vagrant from where Vagrantfile is placed; command line with either
```
#!batch

vagrant up
```
 (for the default provisioner, Hyper-V) or 
```
#!batch

vagrant up -provider virtualbox
```
 (experimental, didn't work on Windows 10)